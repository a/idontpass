import os
import sys
from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import pkcs7


def gen_signature(
    relevant_dir: str,
    manifest: bytes,
    caintermediate_path: bytes,
    pass_cert_path: bytes,
    pass_key_path: bytes,
    write: bool = False,
) -> bytes:
    with open(caintermediate_path, "rb") as f:
        ca_intermediate = f.read()

    with open(pass_cert_path, "rb") as f:
        pass_cert = f.read()

    with open(pass_key_path, "rb") as f:
        pass_key = f.read()

    cert = x509.load_pem_x509_certificate(pass_cert)
    caintermediatecert = x509.load_pem_x509_certificate(ca_intermediate)
    key = serialization.load_pem_private_key(pass_key, None)
    options = [pkcs7.PKCS7Options.DetachedSignature]
    sigbytes = (
        pkcs7.PKCS7SignatureBuilder()
        .set_data(manifest)
        .add_signer(cert, key, hashes.SHA1())
        .add_certificate(caintermediatecert)
        .sign(serialization.Encoding.DER, options)
    )

    if write:
        with open(os.path.join(relevant_dir, "signature"), "wb") as f:
            f.write(sigbytes)
    return sigbytes


if __name__ == "__main__":
    relevant_dir = sys.argv[1]
    cert_path = os.path.join("certs", "pass.pem")
    caintermediate_path = os.path.join("certs", "AppleWWDRCAG4.pem")
    key_path = os.path.join("certs", "pass.key")
    manifest_path = os.path.join(relevant_dir, "manifest.json")

    with open(manifest_path) as f:
        manifest = f.read()

    gen_signature(
        relevant_dir, manifest, caintermediate_path, cert_path, key_path, write=True
    )
