# iDontPass

yet another project aimed at creating apple wallet passes :) expect garbage python, it was written on a single night without any attention for quality code.

## usage

### preparation

- grab your cert from https://developer.apple.com/account/resources/certificates/add (Pass Type ID Certificate)
- convert it to a PEM: `openssl x509 -in pass.cer -inform DER -out pass.pem -outform PEM`, place it under `certs` as `pass.pem`
- place your key under `certs` as `pass.key`
- grab the relevant intermediate certificate from [apple's CA page](https://www.apple.com/certificateauthority/), you'll want one of the "Worldwide Developer Relations" ones
    - you can figure out which one you need by checking which one issued your certificate with `openssl x509 -in pass.pem -text | grep "Issuer:"`, check the OU. At the time of writing they all use G4 and that's hardcoded, which I should probably fix.
- convert the intermediate cert to a PEM: `openssl x509 -in AppleWWDRCAG4.cer -inform DER -out AppleWWDRCAG4.pem -outform PEM`
- install the dependencies of this project, ofc: `pip3 install -Ur requirements.txt`

### actually using the thing

- don't
- if you really want to use it, place your .pass folder in `passes` (or anywhere else idc)
- run `python3 genpkpass.py passes/passnamegoeshere.pass outputnamegoeshere`
- your pkpass will be placed to `output/outputnamegoeshere.pkpass`
- toss that in the general direction of your ios device somehow, and it should hopefully work
- if you want to just generate a manifest or a signature file you can call those files by themselves like `python3 genmanifest.py passes/passnamegoeshere.pass` or `python3 gensig.py passes/passnamegoeshere.pass`

## helpful links

### documentation

- https://developer.apple.com/wallet/get-started/
- https://developer.apple.com/documentation/passkit
- https://developer.apple.com/documentation/walletpasses
- https://developer.apple.com/documentation/walletpasses/creating_the_source_for_a_pass
- https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/PassKit_PG/Creating.html#//apple_ref/doc/uid/TP40012195-CH4-SW1
- https://developer.apple.com/library/archive/documentation/UserExperience/Reference/PassKit_Bundle/Chapters/FieldDictionary.html#//apple_ref/doc/uid/TP40012026-CH4
- https://developer.apple.com/documentation/walletpasses/pass
- https://developer.apple.com/documentation/walletpasses/pass/barcodes
- https://developer.apple.com/documentation/walletpasses/passfieldcontent
- https://developer.apple.com/documentation/walletpasses/building_a_pass
- https://developer.apple.com/documentation/walletpasses/distributing_and_updating_a_pass

### relevant apple dev stuff

- https://developer.apple.com/account/resources/certificates/add (Pass Type ID Certificate)
- https://developer.apple.com/account/resources/identifiers/list/passTypeId

### helpful in general

- https://pkpassvalidator.azurewebsites.net/
- https://github.com/keefmoon/Passbook-Example-Code/tree/master/Pass-Example-Generic/Package
- https://github.com/keefmoon/Passbook-Example-Code/blob/master/Pass-Example-Generic/Pass-Example-Generic.pkpass
- https://github.com/tomasmcguinness/dotnet-passbook/
