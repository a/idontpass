from gensig import gen_signature
from genmanifest import gen_manifest
from zipfile import ZipFile
import os
import sys


def gen_pkpass(relevant_dir: str, output_filename: str):
    cert_path = os.path.join("certs", "pass.pem")
    caintermediate_path = os.path.join("certs", "AppleWWDRCAG4.pem")
    key_path = os.path.join("certs", "pass.key")

    ignored_files = [".DS_Store", "manifest.json", "signature"]

    manifest = gen_manifest(relevant_dir)
    signature = gen_signature(
        relevant_dir, manifest, caintermediate_path, cert_path, key_path
    )

    output_path = os.path.join("output", output_filename + ".pkpass")

    with ZipFile(output_path, "w") as pkpass:
        with pkpass.open("manifest.json", "w") as zipf:
            zipf.write(manifest)
        with pkpass.open("signature", "w") as zipf:
            zipf.write(signature)

        for root, _, files in os.walk(relevant_dir):
            clean_root = root.replace(relevant_dir, "").strip(os.sep)
            for file in files:
                if file in ignored_files:
                    continue

                file_path = os.path.join(root, file)
                clean_file_path = os.path.join(clean_root, file)
                with open(file_path, "rb") as f:
                    with pkpass.open(clean_file_path, "w") as zipf:
                        zipf.write(f.read())
    print(f"Saved to {output_path}!")


if __name__ == "__main__":
    gen_pkpass(sys.argv[1], sys.argv[2])
