import os
import sys
import json
import hashlib


def gen_manifest(
    relevant_dir: str, extra_ignored_files: list = [".DS_Store"], write: bool = False
) -> bytes:
    file_hashes = {}
    ignored_files = extra_ignored_files + ["manifest.json", "signature"]

    for root, _, files in os.walk(relevant_dir):
        apple_root = root.replace(relevant_dir, "").strip(os.sep)
        for file in files:
            if file in ignored_files:
                continue

            file_path = os.path.join(root, file)
            apple_path = f"{apple_root}/{file}" if apple_root else file
            with open(file_path, "rb") as f:
                file_hashes[apple_path] = hashlib.sha1(f.read()).hexdigest()

    if write:
        with open(os.path.join(relevant_dir, "manifest.json"), "w") as f:
            json.dump(file_hashes, f)
    return json.dumps(file_hashes).encode()


if __name__ == "__main__":
    gen_manifest(sys.argv[1], write=True)
